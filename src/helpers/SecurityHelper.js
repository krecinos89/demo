export const getUserSecurity = (publishedDate) => {
    let purchases = JSON.parse(localStorage.getItem("UserPurchases"));
    if (purchases === null || purchases === []) {
        localStorage.setItem("userSecurity", "anonymous_access");
    } else {
        let subscriber = purchases.some((purchase) => {
            let startDate = new Date(purchase.StartDate);
            let endDate = new Date(purchase.EndDate);
            if (publishedDate.getTime() > startDate.getTime() && publishedDate.getTime() < endDate.getTime()) {
                localStorage.setItem("userSecurity", "subscriber_access");
            }

            return (publishedDate.getTime() > startDate.getTime() && publishedDate.getTime() < endDate.getTime());
        });

        if (!subscriber) {
            localStorage.setItem("userSecurity", "member_access");
        }
    }
};