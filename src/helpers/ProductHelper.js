import * as React from "react";

export const getImageGalery = (product) => {
    let gallery = [{original: product.DefaultImage.Large}];
    if (product.Images.length > 0) {
        gallery = product.Images.map((image) => {
            return {original: image.Large}
        });
    }
    return gallery;
};

export const getPrice = (product) => {
    if (product.Price) {
        if (product.Price === "Information non disponible") {
            return <span className="unknown">{product.Price}</span>
        }
        return <span>{product.Price}</span>
    }
    return null;
};

export const getScore = (product) => {
    if (product.ShowScore) {
        if (product.ScoreImage !== "") {
            return <img role={"presentation"} src={product.ScoreImage}/>
        }
        return (
            <div>
                <span>0%</span>
                <span>{product.Score}%</span>
                <span>100%</span>
            </div>
        )
    }
    return null;
};


export const getCategoryProduct = (product) => {
    if (product.ShowProfilesInProductSheet && product.Profiles.length > 0) {

        let profiles = product.Profiles.map((profil, key) => {
            if (profil.Title) {
                return <li key={key}>{profil.Title}</li>
            }
            return null;
        });

        return <ul>{profiles}</ul>

    }
    return null;
};
