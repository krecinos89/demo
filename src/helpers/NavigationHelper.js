import * as React from "react";

// Components
import ArrowNextArticle from "../components/Tools/ArrowNextArticle"
import ArrowPreviousArticle from "../components/Tools/ArrowPreviousArticle"

export const getNextArrowForArticle = (article, listArticles, categories, numEdition, products) => {
    if (article.index < listArticles.length - 1 || categories[article.categoryIndex].productIndex !== null) {
        if (article.lastInCategory) {
            if (categories[article.categoryIndex].productIndex !== null) {
                return (
                    <ArrowNextArticle
                        isProduct={true}
                        nextProduct={products[categories[article.categoryIndex].productIndex]}
                        numEdition={numEdition}
                    />
                )
            }

            return (
                <ArrowNextArticle
                    isCategory={true}
                    nextCategory={categories[article.categoryIndex + 1]}
                    numEdition={numEdition}
                />
            )

        }

        return (
            <ArrowNextArticle
                nextArticle={listArticles[article.index + 1]}
                numEdition={numEdition}
            />
        )

    }

    return null;
};

export const getPrevArrowForArticle = (article, listArticles, categories, numEdition, products) => {
    if (article.index > 0) {
        if (article.firstInCategory) {
            return (
                <ArrowPreviousArticle
                    isCategory={true}
                    prevCategory={categories[article.categoryIndex]}
                    numEdition={numEdition}
                />
            )
        }

        return (
            <ArrowPreviousArticle
                prevArticle={listArticles[article.index - 1]}
                numEdition={numEdition}
            />
        )

    }

    return (
        <ArrowPreviousArticle
            isHome={true}
            numEdition={numEdition}
        />
    )

};

export const getNextArrowForProduct = (product, listProducts, categories, numEdition) => {
    if (product.index < listProducts.length - 1 || categories[product.categoryIndex].index < categories.length - 1) {
        if (product.lastInCategory) {
            return (
                <ArrowNextArticle
                    isCategory={true}
                    nextCategory={categories[product.categoryIndex + 1]}
                    numEdition={numEdition}
                />
            )

        }

        return (
            <ArrowNextArticle
                isProduct={true}
                nextProduct={listProducts[product.index + 1]}
                numEdition={numEdition}
            />
        )

    }

    return null;
};

export const getPrevArrowForProduct = (product, listProducts, listArticles, categories, numEdition) => {
    if (product.firstInCategory) {
        return (
            <ArrowPreviousArticle
                prevArticle={listArticles[categories[product.categoryIndex].lastArticleIndex]}
                numEdition={numEdition}
            />
        )
    }

    return (
        <ArrowPreviousArticle
            isProduct={true}
            prevProduct={listProducts[product.index - 1]}
            numEdition={numEdition}
        />
    )
};

export const getNextArrowForCategoryIndex = (category, listArticles, numEdition) => {
    if (category.articleIndex !== null) {
        return (
            <ArrowNextArticle
                nextArticle={listArticles[category.articleIndex]}
                numEdition={numEdition}
            />
        )
    }

    return null;
};

export const getPrevArrowForCategoryIndex = (category, listArticles, listProducts, listCategories, numEdition) => {
    console.log(category);
    if (listCategories[category.index - 1].lastProductIndex !== null) {
        return (
            <ArrowPreviousArticle
                isProduct={true}
                prevProduct={listProducts[listCategories[category.index - 1].lastProductIndex]}
                numEdition={numEdition}
            />
        )
    }
    if (category.articleIndex !== null) {
        return (
            <ArrowPreviousArticle
                prevArticle={listArticles[category.articleIndex - 1]}
                numEdition={numEdition}
            />
        )
    }

    return null;
};