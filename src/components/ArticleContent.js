import React, {Component} from 'react';
import Swipeable from 'react-swipeable';
import ImageGallery from 'react-image-gallery';

// Components
import NavBarTop from '../components/NavBarTop'
import ArticleIcons from './Tools/ArticleIcons'
import RappelAndCondamnation from './RappelAndCondamnation'

// Helpers
import * as Navigation from "../helpers/NavigationHelper"

// Styles
import '../css/article_content.css'
import "react-image-gallery/styles/css/image-gallery.css";

class ArticleContent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            isFolder: false,
            isList: false,
            isActiveId: 0,
            isSwipeLeft: false,
            isSwipeRight: false,
            active: "Resume",
            disabledSwipe: false,
            fontClass: "font_small",
        };
        this.toggleListMobile = this.toggleListMobile.bind(this);
        this.swipingLeft = this.swipingLeft.bind(this);
        this.swipingRight = this.swipingRight.bind(this);
        this.onSwiped = this.onSwiped.bind(this);
        this.onSelectSubArticle = this.onSelectSubArticle.bind(this);
        this.onResizeTextClick = this.onResizeTextClick.bind(this);
        this.handleBookmark = this.handleBookmark.bind(this);
    }

    componentWillMount() {
        window.scrollTo(0, 0);
        if (this.props.state.app.articles === null) {
            this.props.history.push('/edition/' + this.props.match.params.numEdition);
        } else {
            this.setIsFolder();
            this.setIsList();
        }
    }

    componentDidMount() {
        if (this.props.state.app.articles !== null) {
            this.props.setArticle(this.props.state.app.articles[this.props.match.params.numArticle]);
            this.setState({active: this.props.state.app.articles[this.props.match.params.numArticle].title});
            localStorage.setItem("lastArticle", this.props.match.params.numArticle);
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.location !== prevProps.location) {
            this.props.setArticle(this.props.state.app.articles[this.props.match.params.numArticle]);
            this.setState({active: this.props.state.app.articles[this.props.match.params.numArticle].title});
            localStorage.setItem("lastArticle", this.props.match.params.numArticle);

            this.setIsList();
            this.setIsFolder();
        }
    }

    setIsFolder() {
        let article = this.props.state.app.articles[this.props.match.params.numArticle];
        if (article.articles !== undefined) {
            this.setState({isFolder: true});
        } else {
            this.setState({isFolder: false});
        }
    }

    setIsList() {
        let article = this.props.state.app.articles[this.props.match.params.numArticle];
        if (article.listArticles !== undefined) {
            this.setState({isList: true});
        } else {
            this.setState({isList: false});
        }
    }

    toggleListMobile() {
        this.setState({visible: !this.state.visible});
    }

    onSelectSubArticle(article) {
        this.setState({active: article.title});
        this.props.setArticleFolder(article);
    }

    swipingLeft() {
        this.setState({isSwipeLeft: true});

    }

    swipingRight() {
        this.setState({isSwipeRight: true});
    }


    onSwiped() {
        if (this.state.isSwipeRight) {
            let prev = this.props.state.article.index - 1;
            if (this.props.state.article.index > 0) {
                if (this.props.state.article.firstInCategory) {
                    this.props.history.push('/edition/' + this.props.match.params.numEdition + '/category/' + this.props.state.article.categoryIndex);
                } else {
                    this.props.history.push('/edition/' + this.props.match.params.numEdition + '/article/' + prev);
                }
            } else {
                this.props.history.push('/edition/' + this.props.match.params.numEdition);
            }

            this.setState({isSwipeRight: false});

        } else if (this.state.isSwipeLeft) {
            let next = this.props.state.article.index + 1;
            if (this.props.state.article.index < this.props.state.app.articles.length - 1) {
                if (this.props.state.article.lastInCategory) {
                    let categoryindex = this.props.state.article.categoryIndex + 1;
                    this.props.history.push('/edition/' + this.props.match.params.numEdition + '/category/' + categoryindex);
                } else {
                    this.props.history.push('/edition/' + this.props.match.params.numEdition + '/article/' + next);
                }
            }

            this.setState({isSwipeLeft: false});
        }
    }

    getImageGalery() {
        if (this.props.state.article.gallery.length > 0) {
            let gallery = this.props.state.article.gallery.map((image) => {
                let description = image.description !== null ? image.description : "";
                return {original: image.image_url, description: description}
            });
            return gallery;
        }
    }

    onTouchGallery() {
        this.setState({disabledSwipe: !this.state.disabledSwipe})
    }

    onResizeTextClick(fontClass) {
        this.setState({fontClass});
    }

    handleBookmark(toDelete) {
        let email = localStorage.getItem("UserEmail");
        if (email !== null) {
            if (toDelete) {
                this.props.deleteBookmark(email, this.props.state.article.remote_id);
            } else {
                this.props.addBookmark(email, this.props.state.article.remote_id);
            }
        }
    }

    getVisibilityClassArticle () {
        let userSecurity = localStorage.getItem("userSecurity");
        if (
            this.props.state.article.security === "anonymous_access" ||
            (this.props.state.article.security === "member_access" && (userSecurity === "member_access" || userSecurity === "subscriber_access")) ||
            (this.props.state.article.security === "subscriber_access" && userSecurity === "subscriber_access")
        ) {
            return "content_visible";
        }

        return "content_hide";
    }

    getArticlesFolder(article, categoryClass) {
        let articlesFolder = null;
        if (article.articles !== undefined) {
            let articles = Object.keys(article.articles).map((key) => article.articles[key]);
            articlesFolder = articles.map((articleFolder, key) => {
                let isActive = this.state.active === articleFolder.title ? "active" : "";
                return <li className={"sub_folder" + categoryClass + " " + isActive} key={key}
                           onClick={() => {
                               this.onSelectSubArticle(articleFolder)
                           }}
                >{articleFolder.title}
                </li>
            })
        }
        return articlesFolder;
    }

    getArticlesList(article) {
        let articlesList = null;
        if (article.listArticles !== undefined) {
            let articles = Object.keys(article.listArticles).map((key) => article.listArticles[key]);
            articlesList = articles.map((articleList, key) => {
                return <div key={key}>
                    <RappelAndCondamnation article={articleList}/>
                </div>
            })
        }

        return articlesList;
    }

    getListMobile(categoryClass, articlesFolder) {
        let list_mobile;
        if (this.state.visible) {
            list_mobile =
                <ul>
                    <li className={"sub_folder" + categoryClass + " " + (this.state.active === this.props.state.app.articles[this.props.match.params.numArticle].title ? "active" : "")}
                        onClick={() => {
                            this.onSelectSubArticle(this.props.state.app.articles[this.props.match.params.numArticle])
                        }}
                    >Résumé
                    </li>
                    {articlesFolder}
                </ul>
        }
        return list_mobile;
    }

    render() {

        if (this.props.state.app.articles !== null) {
            let article = this.props.state.app.articles[this.props.match.params.numArticle];
            let categoryClass = article.category.replace(/[^a-zA-Z]/g, "");

            // récupération du sous menu si besoin
            let articlesFolder = this.getArticlesFolder(article, categoryClass);

            // récupération des rappels et condamnations si besoin
            let articlesList = this.getArticlesList(article);

            // fleche de navigation
            let prevArrow = Navigation.getPrevArrowForArticle(article, this.props.state.app.articles, this.props.state.app.categories, this.props.match.params.numEdition, this.props.state.app.products);
            let nextArrow = Navigation.getNextArrowForArticle(article, this.props.state.app.articles, this.props.state.app.categories, this.props.match.params.numEdition, this.props.state.app.products);

            // list_nav Mobile only
            let list_mobile = this.getListMobile(categoryClass, articlesFolder);

            // récupération de la galery d'image si elle existe
            let images = this.getImageGalery();

            return (


                <Swipeable
                    onSwipingLeft={this.swipingLeft}
                    onSwipingRight={this.swipingRight}
                    onSwiped={this.onSwiped}
                    preventDefaultTouchmoveEvent={true}
                    disabled={this.state.disabledSwipe}
                >
                    <NavBarTop categories={this.props.state.app.categories}/>
                    <div className="full-width-tablet">
                        <div className="container">
                            <div className="row">
                                <div className="header_slider">

                                    {prevArrow}

                                    <div className="col-lg-12 col-12 mx-auto d-block no-padding header_hero_img">
                                        <div className="article_hero_img">
                                            <img role="presentation" src={this.props.state.article.image_article_cover}/>
                                            <span className={categoryClass}>{this.props.state.article.category}</span>
                                        </div>
                                    </div>
                                    <div className="description_article">
                                        <div className="row">
                                            <div className="col-md-6 col-lg-8">
                                                <h1>{this.props.state.article.title}</h1>
                                            </div>
                                            <div className="col-sm-6 col-lg-4 text-right">
                                                <span>{this.props.state.article.author}</span>
                                            </div>
                                        </div>
                                    </div>

                                    {nextArrow}

                                </div>
                            </div>
                            <div className="row mTopArticle">
                                <div className="col-lg-2">
                                    <div className={"category_color " + categoryClass}/>
                                    <div className="author">
                                        <span>{this.props.state.article.author}</span>
                                    </div>

                                    <ArticleIcons
                                        remoteId={this.props.state.article.remote_id}
                                        handleBookmark={(toDelete) => this.handleBookmark(toDelete)}
                                        onResizeTextClick={(fontClass) => this.onResizeTextClick(fontClass)}
                                    />

                                    {/* This is only visible on mobile */}
                                    {this.state.isFolder &&
                                    <div className="col-12 no-padding" onClick={this.toggleListMobile}>
                                        <div className={"subfolder_nav_mobile " + categoryClass}>
                                            <p>{(this.props.state.app.articles[this.props.match.params.numArticle].title === this.state.active ? "Résumé" : this.state.active)}</p>
                                            <img role="presentation" src={require('../assets/arrow_down.png')}/>
                                            {list_mobile}
                                        </div>
                                    </div>
                                    }

                                </div>
                                {this.state.isFolder ? (
                                    <div className="col-lg-6">
                                        <div className={ `article_content ${this.state.fontClass} ` + this.getVisibilityClassArticle()}>
                                            <ImageGallery
                                                showThumbnails={false}
                                                showFullscreenButton={false}
                                                showPlayButton={false}
                                                items={images}
                                                onTouchStart={() => this.onTouchGallery()}
                                                onTouchEnd={() => this.onTouchGallery()}
                                            />
                                            <div className="bold">{this.props.state.article.introduction}</div>
                                            <div className="article-text" dangerouslySetInnerHTML={{__html: this.props.state.article.text}}/>
                                        </div>
                                    </div>
                                ) : (
                                    <div className="col-lg-10">
                                        <div className={ `article_content ${this.state.fontClass} ` + this.getVisibilityClassArticle()}>
                                            <ImageGallery
                                                showThumbnails={false}
                                                showFullscreenButton={false}
                                                showPlayButton={false}
                                                items={images}
                                                onTouchStart={() => this.onTouchGallery()}
                                                onTouchEnd={() => this.onTouchGallery()}
                                            />
                                            <div className="bold">{this.props.state.article.introduction}</div>
                                            <div className="article-text" dangerouslySetInnerHTML={{__html: this.props.state.article.text}}/>
                                            {/*CHRINIQUES*/}
                                            {this.state.isList &&
                                                <div className="chroniques">
                                                    {articlesList}
                                                </div>
                                            }
                                        </div>
                                    </div>
                                )}
                                {/* This is only visible on desktop */}
                                {this.state.isFolder &&
                                <div className="col-lg-4 sub_folder_nav">
                                    <ul>
                                        <li className={"sub_folder" + categoryClass + " " + (this.state.active === this.props.state.app.articles[this.props.match.params.numArticle].title ? "active" : "")}
                                            onClick={()=> {this.onSelectSubArticle(this.props.state.app.articles[this.props.match.params.numArticle])}}
                                        >Résumé
                                        </li>
                                        {articlesFolder}

                                    </ul>
                                </div>
                                }
                            </div>
                        </div>
                    </div>
                </Swipeable>
            );
        } else {
            return null;
        }
    }
}

export default ArticleContent;

