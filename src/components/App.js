import React, {Component} from 'react';
import {withCookies, Cookies} from 'react-cookie';
import {instanceOf} from 'prop-types';

import NavBarTop from '../components/NavBarTop';
import ArticleHeader from "../components/ArticleHeader";
import ArticleList from "../components/ArticleList";
import XlLogo from "../components/Tools/XlLogo";

class App extends Component {

    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    componentWillMount() {
        this.connectUserByCookie();
        this.getCatalogue();
    }

    getCatalogue() {
        // recupération du catalogue courant
        let numEdition = this.props.match.params.numEdition;
        this.props.getCatalogue(numEdition);
        localStorage.setItem("lastEdition", numEdition)
    }

    connectUserByCookie() {
        //recupération des données utilisateurs
        const {cookies} = this.props;
        let cookie = cookies.get("MultiPlateformPvUser");
        let localCookie = localStorage.getItem("UserCookie");
        if (cookie === undefined) {
            this.props.disconnect();
        } else if (localCookie === null || cookie !== localCookie) {
            this.props.connectByCookie(cookie);
        }
    }

    render() {
        let articles = this.props.state.app.articles;
        let returnArticles = {
            header: null,
            first: null,
            second: null,
            third: null,
            list: null
        };
        if (articles !== null) {
            returnArticles.list = articles.map((article, key) => {
                switch (key) {
                    case 0 :
                        returnArticles.header = <ArticleHeader article={article}/>;
                        break;
                    case 1 :
                        returnArticles.first = <ArticleList key={key} article={article}/>;
                        break;
                    case 2 :
                        returnArticles.second = <ArticleList key={key} article={article}/>;
                        break;
                    case 3 :
                        returnArticles.third = <ArticleList key={key} article={article}/>;
                        break;
                    default :
                        return (
                            <div key={key} className="col-lg-3 col-md-4 col-sm-6 col-6">
                                <ArticleList key={key} article={article}/>
                            </div>
                        )
                }
                return null;
            })
        }

        return (

                <div className="App">

                    {this.props.state.app.categories !== null &&
                    <NavBarTop user={this.props.state.app.user} categories={this.props.state.app.categories}/>

                    }
                    <div className="full-width-responsive">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-12">
                                    <XlLogo catalogueName={this.props.state.app.catalogueName}/>
                                </div>
                                    <div className="col-lg-9 col-md-12 col-12 margin_top_index  mtopipad">
                                        {returnArticles.header}
                                    </div>
                                <div className="d-none d-lg-block d-xl-block col-lg-3 ">
                                    {returnArticles.first}
                                    {returnArticles.second}
                                    {returnArticles.third}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="grey_bg">
                        <div className="container">
                            <div className="row">
                                <div className="d-lg-none col-lg-3 col-md-4 col-sm-6 col-6">
                                    {returnArticles.first}
                                </div>
                                <div className="d-lg-none col-lg-3 col-md-4 col-sm-6 col-6">
                                    {returnArticles.second}
                                </div>
                                <div className="d-lg-none col-lg-3 col-md-4 col-sm-6 col-6">
                                    {returnArticles.third}
                                </div>
                                {returnArticles.list}
                            </div>
                        </div>
                    </div>
                </div>

        );
    }
}


export default withCookies(App);

