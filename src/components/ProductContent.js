import React, {Component} from 'react';
import ImageGallery from 'react-image-gallery';
// import { StickyContainer, Sticky } from 'react-sticky';

// Components
import NavBarTop from '../components/NavBarTop';

// Helpers
import * as Navigation from "../helpers/NavigationHelper"
import * as ProductHelper from "../helpers/ProductHelper"

// style
import '../css/product_content.css'

class ProductContent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
            active: "Resume",
            scrolling: false
        };

        this.toggleCharacteristicsList = this.toggleCharacteristicsList.bind(this);
        this.getCharacteristicsList = this.getCharacteristicsList.bind(this);
        this.handleScroll = this.handleScroll.bind(this);
    }


    handleScroll() {
        if (window.scrollY === 0 && this.state.scrolling === true) {
            this.setState({scrolling: false});
            console.log("you are scrolling up 100px")
        }
        else if (window.scrollY !== 0 && this.state.scrolling !== true) {
            this.setState({scrolling: true});
            console.log(" you are scrolling down ")
        }
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    componentWillMount() {
        window.scrollTo(0, 0);
        if (this.props.state.app.products === null) {
            this.props.history.push('/edition/' + this.props.match.params.numEdition);
        }

        window.removeEventListener('scroll', this.handleScroll);
    }

    toggleCharacteristicsList() {
        this.setState({isVisible: !this.state.isVisible});
    }



    getCharacteristicsList() {
        let characteristicsList;
        if (this.state.isVisible) {
            characteristicsList =
                <ul className="characteristics_list">
                    <li>
                        Résumé
                    </li>
                </ul>
        }

        return characteristicsList;
    }

    render() {
        if (this.props.state.app.products !== null) {
            let product = this.props.state.app.products[this.props.match.params.numProduct];

            // This is the characteristics list
            let characteristicsList = this.getCharacteristicsList();

            // fleche de navigation
            let prevArrow = Navigation.getPrevArrowForProduct(product, this.props.state.app.products, this.props.state.app.articles, this.props.state.app.categories, this.props.match.params.numEdition);
            let nextArrow = Navigation.getNextArrowForProduct(product, this.props.state.app.products, this.props.state.app.categories, this.props.match.params.numEdition);

            // récupération des informations du produit
            let images = ProductHelper.getImageGalery(product);
            let price = ProductHelper.getPrice(product);
            let score = ProductHelper.getScore(product);
            let categoryProduct = ProductHelper.getCategoryProduct(product);

            return (
                <div>
                    <NavBarTop categories={this.props.state.app.categories}/>

                    <div className="products_sheet">
                        <div className="container container--full-width">
                            {prevArrow}
                            <div className="row no-gutters-mobile">
                                    <div className="presentation_sticky_col " onScroll={() => this.handleScroll()}>
                                        <div className="product_presentation">
                                            <ImageGallery
                                                showThumbnails={false}
                                                showFullscreenButton={false}
                                                showPlayButton={false}
                                                items={images}
                                                // onTouchStart={() => this.onTouchGallery()}
                                                // onTouchEnd={() => this.onTouchGallery()}
                                            />
                                            <div className="product_description">
                                                <h2>{product.Name}</h2>
                                                <span>{price}</span>
                                            </div>

                                            <div className="product_rating">
                                                {score}
                                                {/*TODO tooltips pour le score*/}

                                            </div>
                                        </div>
                                    </div>
                                    <div className="presentation_info_col bg">
                                        <div className="product_info_list">
                                            <div className="characteristics_container" onClick={() => this.toggleCharacteristicsList()}>
                                                <img className="resume_icon" role="presentation" src={require('../assets/icn_resume.svg')}/>
                                                <p>Résumé</p>
                                                {/*<p>{(this.props.state.app.articles[this.props.match.params.numArticle].title === this.state.active ? "Résumé" : this.state.active)}</p>*/}
                                                <img className="arrow_icon" role="presentation" src={require('../assets/arrow_down.png')}/>
                                                {characteristicsList}
                                            </div>
                                        </div>
                                        <div className="product_information">
                                            {categoryProduct}
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            A adipisci corporis deleniti, dolor eveniet expedita hic in ipsum
                                            minus mollitia, placeat sed totam. Eaque eveniet laudantium quis.
                                            Illum, sequi voluptatum.
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            A adipisci corporis deleniti, dolor eveniet expedita hic in ipsum
                                            minus mollitia, placeat sed totam. Eaque eveniet laudantium quis.
                                            Illum, sequi voluptatum.
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            A adipisci corporis deleniti, dolor eveniet expedita hic in ipsum
                                            minus mollitia, placeat sed totam. Eaque eveniet laudantium quis.
                                            Illum, sequi voluptatum.
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            A adipisci corporis deleniti, dolor eveniet expedita hic in ipsum
                                            minus mollitia, placeat sed totam. Eaque eveniet laudantium quis.
                                            Illum, sequi voluptatum.
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            A adipisci corporis deleniti, dolor eveniet expedita hic in ipsum
                                            minus mollitia, placeat sed totam. Eaque eveniet laudantium quis.
                                            Illum, sequi voluptatum.
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            A adipisci corporis deleniti, dolor eveniet expedita hic in ipsum
                                            minus mollitia, placeat sed totam. Eaque eveniet laudantium quis.
                                            Illum, sequi voluptatum.
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            A adipisci corporis deleniti, dolor eveniet expedita hic in ipsum
                                            minus mollitia, placeat sed totam. Eaque eveniet laudantium quis.
                                            Illum, sequi voluptatum.
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            A adipisci corporis deleniti, dolor eveniet expedita hic in ipsum
                                            minus mollitia, placeat sed totam. Eaque eveniet laudantium quis.
                                            Illum, sequi voluptatum.v

                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            A adipisci corporis deleniti, dolor eveniet expedita hic in ipsum
                                            minus mollitia, placeat sed totam. Eaque eveniet laudantium quis.
                                            Illum, sequi voluptatum.
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            A adipisci corporis deleniti, dolor eveniet expedita hic in ipsum
                                            minus mollitia, placeat sed totam. Eaque eveniet laudantium quis.
                                            Illum, sequi voluptatum.
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            A adipisci corporis deleniti, dolor eveniet expedita hic in ipsum
                                            minus mollitia, placeat sed totam. Eaque eveniet laudantium quis.
                                            Illum, sequi voluptatum.                                            
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            A adipisci corporis deleniti, dolor eveniet expedita hic in ipsum
                                            minus mollitia, placeat sed totam. Eaque eveniet laudantium quis.
                                            Illum, sequi voluptatum.                                            
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            A adipisci corporis deleniti, dolor eveniet expedita hic in ipsum
                                            minus mollitia, placeat sed totam. Eaque eveniet laudantium quis.
                                            Illum, sequi voluptatum.                                            
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            A adipisci corporis deleniti, dolor eveniet expedita hic in ipsum
                                            minus mollitia, placeat sed totam. Eaque eveniet laudantium quis.
                                            Illum, sequi voluptatum.                                            
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            A adipisci corporis deleniti, dolor eveniet expedita hic in ipsum
                                            minus mollitia, placeat sed totam. Eaque eveniet laudantium quis.
                                            Illum, sequi voluptatum.

                                        </div>
                                    </div>
                            </div>
                            {nextArrow}
                        </div>
                    </div>
                </div>
            );
        } else {
            return null;
        }
    }
}

export default ProductContent;