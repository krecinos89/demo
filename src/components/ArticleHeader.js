import React, {Component} from 'react';
import {Link} from 'react-router-dom';

// style
import '../css/header_article.css';

class ArticleHeader extends Component {

    render() {
        let categoryClass = this.props.article.category.replace(/[^a-zA-Z]/g, "");
        return (
            <div className="row">
                    <div className="article_header">
                        <Link to={"/edition/" + localStorage.getItem("lastEdition") + "/article/" + this.props.article.index}>
                            <div className="hero_img">
                                <img role="presentation" src={this.props.article.image_big}/>
                                <span className={"uppercase " + categoryClass}>{this.props.article.category}</span>
                            </div>
                            <div className="description">
                                <h1 className={"uppercase"}>{this.props.article.title}</h1>
                            </div>
                        </Link>
                    </div>
            </div>
        );
    }
}

export default ArticleHeader;
