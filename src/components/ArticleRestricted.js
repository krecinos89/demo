import React, {Component} from 'react';


// style
import '../css/article_restricted.css';



class ArticleRestricted extends Component {

    render() {
        return (

                <div className="bg">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="content">
                                    <img role="presentation" src={require('../assets/icn-magazine.png')}/>
                                    <p>BIENVENUE SUR LA VISIONEUSE</p>
                                    <p>Connectez-vous pour lire à cette article<br/>et accéder à de nombreux contenus</p>
                                    <button className="btn_login">ME CONNECTER</button>
                                    <div className="boutique">
                                        <a href="">Accéder à la boutique</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        );
    }
}

export default ArticleRestricted;