import {Component} from 'react';


// style
import '../css/article_list.css';

class Default extends Component {

    componentWillMount() {
        let edition = localStorage.getItem("lastEdition");
        let article = localStorage.getItem("lastArticle");
        if (edition !== null) {
            if (article !== null) {
                this.props.history.push('/edition/' + edition + '/article/' + article);
            } else {
                this.props.history.push('/edition/' + edition);
            }
        }
    }

    render() {
        return null;
    }
}

export default Default;
