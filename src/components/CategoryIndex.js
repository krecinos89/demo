import React, {Component} from 'react';
import Swipeable from 'react-swipeable';

// Components
import NavBarTop from '../components/NavBarTop';
import ArticleHeader from "../components/ArticleHeader";
import ArticleList from "../components/ArticleList";

// Helpers
import * as Navigation from "../helpers/NavigationHelper"

// Styles
import '../css/index_category.css'

class CategoryIndex extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isSwipeLeft: false,
            isSwipeRight: false,
        };

        this.swipingLeft = this.swipingLeft.bind(this);
        this.swipingRight = this.swipingRight.bind(this);
        this.onSwiped = this.onSwiped.bind(this);
    }

    componentWillMount() {
        if (this.props.state.app.categories === null) {
            this.props.history.push('/edition/' + this.props.match.params.numEdition);
        } else {
            this.props.setCategory(this.props.state.app.categories[this.props.match.params.numCategory]);
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.location !== prevProps.location) {
            this.props.setCategory(this.props.state.app.categories[this.props.match.params.numCategory]);
        }
    }

    swipingLeft() {
        this.setState({isSwipeLeft: true});
    }

    swipingRight() {
        this.setState({isSwipeRight: true});
    }

    onSwiped() {
        if (this.props.state.category.articleIndex !== null) {
            if (this.state.isSwipeRight) {
                let prev = this.props.state.category.articleIndex - 1;
                this.props.history.push('/edition/' + this.props.match.params.numEdition + '/article/' + prev);
                this.setState({isSwipeRight: false})
            } else if (this.state.isSwipeLeft) {
                let next = this.props.state.category.articleIndex;
                this.props.history.push('/edition/' + this.props.match.params.numEdition + '/article/' + next);
                this.setState({isSwipeLeft: false})
            }
        }
    }

    render() {
        let category = this.props.state.category;
        let returnArticles = {
            header: null,
            first: null,
            second: null,
            third: null,
            list: null
        };
        if (category.articles !== null) {
            returnArticles.list = category.articles.map((article, key) => {
                switch (key) {
                    case 0 :
                        returnArticles.header = <ArticleHeader article={article}/>;
                        break;
                    case 1 :
                        returnArticles.first = <ArticleList key={key} article={article}/>;
                        break;
                    case 2 :
                        returnArticles.second = <ArticleList key={key} article={article}/>;
                        break;
                    case 3 :
                        returnArticles.third = <ArticleList key={key} article={article}/>;
                        break;
                    default :
                        return (
                            <div key={key} className="col-lg-3 col-md-4 col-sm-6 col-6">
                                <ArticleList key={key} article={article}/>
                            </div>
                        )
                }
                return null;
            })
        }

        let categoryClass = null;
        if (category.title !== null) {
            categoryClass = this.props.state.category.title.replace(/[^a-zA-Z]/g, "");
        }

        // fleche de navigation
        let prevArrow = null;
        let nextArrow = null;
        if (category.index !== null) {
            console.log(category);
            prevArrow = Navigation.getPrevArrowForCategoryIndex(category, this.props.state.app.articles, this.props.state.app.products, this.props.state.app.categories, this.props.match.params.numEdition);
            nextArrow = Navigation.getNextArrowForCategoryIndex(category, this.props.state.app.articles, this.props.match.params.numEdition);
        }

        return (
            <Swipeable
                onSwipingLeft={this.swipingLeft}
                onSwipedRight={this.swipingRight}
                onSwiped={this.onSwiped}
            >

                <div className="App">

                    {this.props.state.app.categories !== null &&
                    <NavBarTop user={this.props.state.app.user} categories={this.props.state.app.categories} categoryClass={categoryClass} categoryTitle={category.title}/>

                    }

                    <div className="full-width-responsive">
                        <div className="container">
                            <div className="row rel">
                                <div className="index_arrow_prev">
                                    {prevArrow}
                                </div>

                                <div className={categoryClass + "titleCategory col-12 no-padding"}>
                                    {category.title}
                                </div>
                                <div className="col-lg-9 col-md-12 col-12 mTop mtopipadDouble">
                                    {returnArticles.header}
                                </div>
                                <div className="d-none d-lg-block d-xl-block col-lg-3 mtopipad">
                                    {returnArticles.first}
                                    {returnArticles.second}
                                    {returnArticles.third}
                                </div>
                            </div>
                            <div className="index_arrow_next">
                                {nextArrow}
                            </div>
                        </div>
                    </div>

                    <div className="grey_bg">
                        <div className="container">
                            <div className="row">
                                <div className="d-lg-none col-lg-3 col-md-4 col-sm-6 col-6">
                                    {returnArticles.first}
                                </div>
                                <div className="d-lg-none col-lg-3 col-md-4 col-sm-6 col-6">
                                    {returnArticles.second}
                                </div>
                                <div className="d-lg-none col-lg-3 col-md-4 col-sm-6 col-6">
                                    {returnArticles.third}
                                </div>
                                {returnArticles.list}
                            </div>
                        </div>
                    </div>

                </div>
            </Swipeable>
        );
    }
}


export default CategoryIndex;

