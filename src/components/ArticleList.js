import React, {Component} from 'react';
import {Link} from 'react-router-dom';

// style
import '../css/article_list.css';

class ArticleList extends Component {

    render() {
        let categoryClass = this.props.article.category.replace(/[^a-zA-Z]/g, "");

        return (
            <div className="row">
                <div className="article_list">
                    <Link
                        to={"/edition/" + localStorage.getItem("lastEdition") + "/article/" + this.props.article.index}>
                        <div className="article_list_img">
                            <img role="presentation" src={this.props.article.image_mini}/>
                            <span className={"uppercase " + categoryClass}>{this.props.article.category}</span>
                        </div>
                        <div className="article_title">
                            <h2 className={"uppercase"}>{this.props.article.title}</h2>
                        </div>
                    </Link>
                </div>
            </div>
        );
    }
}

export default ArticleList;
