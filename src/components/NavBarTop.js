import React, {Component} from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {Link} from 'react-router-dom';
import {withCookies, Cookies} from 'react-cookie';
import {instanceOf} from 'prop-types'


// style
import '../css/nav_bar_top.css'
import '../css/dropdown_menu.css'
import '../css/profile_menu.css'
import '../css/App.css'


class NavBarTop extends Component {
    constructor(props) {
        super(props);
        this.toggleMenu = this.toggleMenu.bind(this);
        this.toggleMenuProfile = this.toggleMenuProfile.bind(this);
        this.toggleMenuMobile = this.toggleMenuMobile.bind(this);
        this.state = {
            menuActive: false,
            menuActiveProfile: false,
            visible: false,
            menuClass: "closed",
            transitionRotateClass: "transition_default"
        };
    }

    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    toggleMenu() {
        let isMenuActive = !this.state.menuActive;
        this.setState({
            menuActive: isMenuActive,
            menuClass: isMenuActive ? "open" : "closed",
        });
    }

    toggleMenuProfile() {
        let menuState = !this.state.menuActiveProfile;
        let transition = (this.state.transitionRotateClass === "transition_default") ? "transition_active" : "transition_default";
        this.setState({
            menuActiveProfile: menuState,
            transitionRotateClass: transition
        });
    }

    toggleMenuMobile() {
        this.setState({
            visible: !this.state.visible

        });
    }

    getMenuCategories() {
        let categories = Object.keys(this.props.categories).map((key) => this.props.categories[key]);
        let menuCategories = categories.map((category, key) => {
            if (category.articleIndex === null) {
                return null;
            }
            let categoryClass = category.name.replace(/[^a-zA-Z]/g, "");
            let link = null;
            if (key === 0) {
                link = "/edition/" + localStorage.getItem("lastEdition");
            } else {
                link = "/edition/" + localStorage.getItem("lastEdition") + "/category/" + key
            }
            return (
                <Link key={key}
                      to={link}>
                    <li className={"uppercase " + categoryClass}>
                        {category.name}
                    </li>
                </Link>
            )
        });
        return menuCategories;
    }


    getMenuMobile(menuCategories) {
        let menu_mobile;
        let visibility = "hide";
        if (this.state.visible) {
            visibility = "show";

            menu_mobile =
                <div className="slide_down_list">
                    <ul>
                        {menuCategories}
                        <li className="uppercase bg_black">accédez à votre vitrine</li>
                    </ul>
                </div>

        }
        return {menu_mobile, visibility};
    }

    getMenuProfile() {
        let menu_profile;
        if (this.state.menuActiveProfile) {
            menu_profile =
                <div>
                    <ul>
                        <li>Mon compte</li>
                        <li onClick={() => this.logOut()}>Déconnexion</li>
                    </ul>
                </div>
        } else {
            menu_profile = "";
        }
        return menu_profile;
    }

    getMenuDesktop(menuCategories) {
        let menu;
        if (this.state.menuActive) {
            menu =
                <div>
                    <ul>
                        {menuCategories}
                        <li className="uppercase bg_black">accédez à votre vitrine</li>
                    </ul>
                </div>
        } else {
            menu = "";
        }
        return menu;
    }

    logOut() {
        const {cookies} = this.props;

        localStorage.removeItem("UserCookie");
        localStorage.removeItem("UserEmail");
        localStorage.removeItem("UserFirstName");
        localStorage.removeItem("UserLastName");
        localStorage.removeItem("UserFullName");
        localStorage.removeItem("UserPurchases");
        localStorage.removeItem("UserImageUrl");
        localStorage.removeItem("UserBookmark");

        cookies.remove("MultiPlateformPvUser", {path: '/', domain: '.protegezvous-ezplatform.dev.kaliop.ca'});
    }

    render() {
        let menuCategories = this.getMenuCategories();
        let menu = this.getMenuDesktop(menuCategories);
        let menu_profile = this.getMenuProfile();
        let {menu_mobile, visibility} = this.getMenuMobile(menuCategories);

        return (
            <div className={"container-fluid " + this.props.categoryClass}>
                <div className="row">
                    <div className="col-6 col-lg-3  no-padding">
                        <div className={"menu-burger float-left"} onClick={this.toggleMenu}>
                            <div className={`nav_burger_icon ${this.state.menuClass}`}>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <div id="menu">
                                <ReactCSSTransitionGroup
                                    transitionName="menu"
                                    transitionEnterTimeout={500}
                                    transitionLeaveTimeout={300}>
                                    {menu}
                                </ReactCSSTransitionGroup>
                            </div>
                        </div>
                        {/* MENU MOBILE */}
                        <div className={"menu-burger-mobile " + this.props.categoryClass}
                             onClick={this.toggleMenuMobile}>
                            <div className="nav_burger_icon">
                                <img role="presentation" src={require('../assets/burger_icon.png')}/>
                            </div>
                            <div id="menu_mobile" className={visibility}>
                                <div className="logo-pv-slide_nav">
                                    <Link to={"/edition/" + localStorage.getItem("lastEdition")}>
                                        <img role="presentation" src={require('../assets/logo-pv.svg')}/>
                                    </Link>
                                </div>

                                <div className="slide_nav_burger_icon">
                                    <img role="presentation" src={require('../assets/burger_icon_closed.png')}/>
                                </div>
                            </div>
                            <ReactCSSTransitionGroup
                                transitionName="menu_mobile"
                                transitionEnterTimeout={500}
                                transitionLeaveTimeout={300}>
                                {menu_mobile}
                            </ReactCSSTransitionGroup>
                        </div>
                        {/* MENU MOBILE END */}
                        <div className="logo-pv float-left">
                            <Link to={"/edition/" + localStorage.getItem("lastEdition")}>
                                <img role="presentation" src={require('../assets/logo-pv.svg')}/>
                            </Link>
                        </div>
                    </div>
                    <div className="col-6 col-lg-9">
                            <div className="sign-in" onClick={this.toggleMenuProfile}>
                                    <a className="sign_in_link">
                                        <img role="presentation" src={require('../assets/icon_pv_profile.svg')}/>
                                        {(localStorage.getItem("UserFirstName") && localStorage.getItem("UserLastName")) ? (
                                            <p>{localStorage.getItem("UserFirstName")} {localStorage.getItem("UserLastName")} </p>
                                        ) : (
                                            <p>Aller à mon compte </p>
                                        )}
                                        <img className={ `arrow_down ${this.state.transitionRotateClass}`} role="presentation" src={require('../assets/down-arrow.svg')}/>
                                    </a>
                                    <div className="profile_menu">
                                        <ReactCSSTransitionGroup
                                            transitionName="menu_profile"
                                            transitionEnterTimeout={500}
                                            transitionLeaveTimeout={300}>
                                            {menu_profile}
                                        </ReactCSSTransitionGroup>
                                    </div>
                            </div>
                    </div>
                </div>
                <div className="category_mobile">
                    {this.props.categoryTitle}
                </div>
            </div>
        );
    }
}

export default withCookies(NavBarTop);
