import React, {Component} from 'react';

class ArticleIcons extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fontSize: "font_small",
            isBookmark: false,
        };

        this.resizeText = this.resizeText.bind(this);
        this.changeBookmark = this.changeBookmark.bind(this);
    }

    componentWillMount() {
        this.setState({isBookmark: this.checkIsBookmark(this.props.remoteId)})
    }

    componentDidUpdate(prevProps) {
        if (prevProps.remoteId !== this.props.remoteId) {
            this.setState({isBookmark: this.checkIsBookmark(this.props.remoteId)})
        }
    }

    resizeText() {
        const {fontSize} = this.state;
        let fontClass = "font_small";

        switch (true) {
            case (fontSize === "font_small"):
                fontClass = "font_medium";
                break;
            case (fontSize === "font_medium"):
                fontClass = "font_large";
                break;
            case (fontSize === "font_large"):
            default:
                fontClass = "font_small";
                break;
        }

        this.setState({fontSize: fontClass}, this.props.onResizeTextClick(fontClass));
    }

    changeBookmark (toDelete) {
        if (localStorage.getItem("UserBookmark") !== null) {
            this.props.handleBookmark(toDelete);
            this.setState({isBookmark: !toDelete});
        }
    }

    checkIsBookmark(remoteId) {
        let bookmarks = localStorage.getItem("UserBookmark");
        return bookmarks !== null && bookmarks.indexOf(remoteId) >= 0;
    }

    getBookmarkIcon() {
        if (this.props.remoteId === undefined) {
            return null;
        }

        if (this.state.isBookmark ) {
            return (
                <a onClick={() => this.changeBookmark(true)}>
                    <img role="presentation" src={require('../../assets/bookmark_article_icon_red.png')}/>
                </a>
            )
        } else {
            return (
                <a onClick={() => this.changeBookmark(false)}>
                    <img role="presentation" src={require('../../assets/bookmark_article_icon.png')}/>
                </a>
            )
        }
    }

    render() {

        let bookmarkIcon = this.getBookmarkIcon();

        return (
            <div className="article_icons">
                {bookmarkIcon}
                <a onClick={() => window.print()}>
                    <img role="presentation" src={require('../../assets/article_icon_print.png')}/>
                </a>
                <a onClick={() => this.resizeText()}>
                    <img role="presentation" src={require('../../assets/icn_text_size.png')}/>
                </a>
            </div>
        )
    }
}

export default ArticleIcons;
