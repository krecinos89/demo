import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

class ArrowNextArticles extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isHoveringNext: false,
        };
        this.handleMouseHoverNext = this.handleMouseHoverNext.bind(this);
    }

    handleMouseHoverNext() {
        this.setState({isHoveringNext: !this.state.isHoveringNext});
    }

    getNextTitle() {
        if (this.state.isHoveringNext) {
            if (this.props.isCategory) {
                return (
                    <div className="next_article">
                        <span>prochaine Categorie</span>
                        <p>{this.props.nextCategory.name}</p>
                    </div>
                )
            } else if (this.props.isProduct) {
                return (
                    <div className="next_article">
                        <span>prochain Produit</span>
                        <p>{this.props.nextProduct.Name}</p>
                    </div>
                )
            } else {
                return (
                    <div className="next_article">
                        <span>PROCHAIN ARTICLE</span>
                        <p>{this.props.nextArticle.title}</p>
                    </div>
                )
            }
        }

        return null;
    }

    render() {

        let nextTitle = this.getNextTitle();

        return (
            <div
                className="arrow_next"
                onMouseEnter={this.handleMouseHoverNext}
                onMouseLeave={this.handleMouseHoverNext}
            >
                <ReactCSSTransitionGroup
                    transitionName="hover_effect"
                    transitionEnterTimeout={500}
                    transitionLeaveTimeout={300}>
                    {this.props.isCategory ? (
                        <Link
                            to={"/edition/" + this.props.numEdition + "/category/" + (this.props.nextCategory.index)}>
                            {nextTitle}
                        </Link>
                    ): this.props.isProduct ? (
                        <Link
                            to={"/edition/" + this.props.numEdition + "/product/" + (this.props.nextProduct.index)}>
                            {nextTitle}
                        </Link>
                    ): (
                        <Link
                            to={"/edition/" + this.props.numEdition + "/article/" + (this.props.nextArticle.index)}>
                            {nextTitle}
                        </Link>
                    )}
                </ReactCSSTransitionGroup>

                {this.props.isCategory ? (
                    <Link
                        to={"/edition/" + this.props.numEdition + "/category/" + (this.props.nextCategory.index)}>
                        <img role="presentation" src={require('../../assets/arrow_right.png')}/>
                    </Link>
                ):  this.props.isProduct ? (
                    <Link
                        to={"/edition/" + this.props.numEdition + "/product/" + (this.props.nextProduct.index)}>
                        <img role="presentation" src={require('../../assets/arrow_right.png')}/>
                    </Link>
                ): (
                    <Link
                        to={"/edition/" + this.props.numEdition + "/article/" + (this.props.nextArticle.index)}>
                        <img role="presentation" src={require('../../assets/arrow_right.png')}/>
                    </Link>
                )}

            </div>
        )
    }
}

export default ArrowNextArticles;
