import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

class ArrowPreviousArticle extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isHoveringPrev: false,
        };
        this.handleMouseHoverPrev = this.handleMouseHoverPrev.bind(this);
    }

    handleMouseHoverPrev() {
        this.setState({isHoveringPrev: !this.state.isHoveringPrev});
    }

    getPrevTitle() {
        if (this.state.isHoveringPrev) {
            if (this.props.isHome) {
                return (
                    <div className="prev_article">
                        <span>Accueil</span>
                    </div>
                )
            } else if(this.props.isCategory) {
                return (
                    <div className="prev_article">
                        <span>Categorie Précedente</span>
                        <p>{this.props.prevCategory.name}</p>
                    </div>
                )
            } else if (this.props.isProduct) {
                return (
                    <div className="prev_article">
                        <span>produit Précédent</span>
                        <p>{this.props.prevProduct.Name}</p>
                    </div>
                )
            } else {
                return (
                    <div className="prev_article">
                        <span>ARTICLE PRECEDENT</span>
                        <p>{this.props.prevArticle.title}</p>
                    </div>
                )
            }

        }
        return null;
    }

    render() {

        let nextTitle = this.getPrevTitle();

        return (
            <div
                className="arrow_prev"
                onMouseEnter={this.handleMouseHoverPrev}
                onMouseLeave={this.handleMouseHoverPrev}
            >
                {this.props.isHome ? (
                    <Link
                        to={"/edition/" + this.props.numEdition}>
                        <img role="presentation" src={require('../../assets/arrow_left.png')}/>
                    </Link>
                    ): this.props.isCategory ? (
                    <Link
                        to={"/edition/" + this.props.numEdition + "/category/" + (this.props.prevCategory.index)}>
                        <img role="presentation" src={require('../../assets/arrow_left.png')}/>
                    </Link>
                ): this.props.isProduct ? (
                    <Link
                        to={"/edition/" + this.props.numEdition + "/product/" + (this.props.prevProduct.index)}>
                        <img role="presentation" src={require('../../assets/arrow_left.png')}/>
                    </Link>
                ):(
                    <Link
                        to={"/edition/" + this.props.numEdition + "/article/" + (this.props.prevArticle.index)}>
                        <img role="presentation" src={require('../../assets/arrow_left.png')}/>
                    </Link>
                )}

                <ReactCSSTransitionGroup
                    transitionName="hover_effect"
                    transitionEnterTimeout={500}
                    transitionLeaveTimeout={300}>
                    {this.props.isHome ? (
                        <Link
                            to={"/edition/" + this.props.numEdition}>
                            {nextTitle}
                        </Link>
                        ): this.props.isCategory ? (
                        <Link
                            to={"/edition/" + this.props.numEdition + "/category/" + (this.props.prevCategory.index)}>
                            {nextTitle}
                        </Link>
                    ): this.props.isProduct ? (
                        <Link
                            to={"/edition/" + this.props.numEdition + "/product/" + (this.props.prevProduct.index)}>
                            {nextTitle}
                        </Link>
                    ):(
                        <Link
                            to={"/edition/" + this.props.numEdition + "/article/" + (this.props.prevArticle.index)}>
                            {nextTitle}
                        </Link>
                    )}
                </ReactCSSTransitionGroup>
            </div>
        )
    }
}

export default ArrowPreviousArticle;
