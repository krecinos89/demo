import React, { Component } from 'react';
import {Link} from 'react-router-dom';

class XlLogo extends Component {
    render() {

        return (
            <div className="xl_pv_logo">
                <Link to={"/edition/" + localStorage.getItem("lastEdition")}>
                    <img role="presentation" src={require('../../assets/logo-pv.svg')}/>
                </Link>
                <p className={"uppercase"}>{this.props.catalogueName}</p>
            </div>
        );
    }
}

export default XlLogo;





