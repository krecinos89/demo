import React, {Component} from 'react';


// style
import '../css/article_content.css'

class RappelAndCondamnation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isContentVisible: false,
            transitionRotateClass: "transition_default"
        };

        this.toggleChroniqueContent = this.toggleChroniqueContent.bind(this);
    }

    toggleChroniqueContent() {
        let transition = (this.state.transitionRotateClass === "transition_default") ? "transition_active" : "transition_default";

        this.setState({
            isContentVisible: !this.state.isContentVisible,
            transitionRotateClass: transition
        });
    }

    render() {
        return (
            <div>
                    {this.props.article.image &&
                         <img role="presentation" src={this.props.article.image}/>
                    }
                    <div className="row">
                        <div className="col-10">
                            <div className="chroniques_title">{this.props.article.title}</div>
                            <div className="chroniques_intro">{this.props.article.introduction}</div>
                        </div>
                        <div className="col-2">
                            <img onClick={() => this.toggleChroniqueContent()} className={ `trigger_arrow ${this.state.transitionRotateClass}`}
                                 role="presentation" src={require('../assets/down-arrow.svg')}
                            />
                        </div>
                    </div>
                    {this.state.isContentVisible &&
                        <div className="article-text chroniques_content" dangerouslySetInnerHTML={{__html: this.props.article.text}}/>
                    }
            </div>
        );
    }
}

export default RappelAndCondamnation;
