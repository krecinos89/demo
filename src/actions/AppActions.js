import * as types from './types/AppType';

export function setCatalogue(catalogue) {
    return {
        type: types.SETCATALOGUE,
        data: {
            catalogue: catalogue
        }
    }
}

export function setCategories(categories) {
    return {
        type: types.SETCATEGORIES,
        data: {
            categories: categories
        }
    }
}

export function setArticles(articles) {
    return {
        type: types.SETARTICLES,
        data: {
            articles: articles
        }
    }
}

export function setProducts(products) {
    return {
        type: types.SETPRODUCTS,
        data: {
            products: products
        }
    }
}

export function setCatalogueName(catalogueName) {
    return {
        type: types.SETCATALOGUENAME,
        data: {
            catalogueName: catalogueName
        }
    }
}