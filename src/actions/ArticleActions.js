import * as types from './types/ArticleType';

export function setArticle(article) {
    let author = article.author === null ? null : article.author[0];
    return {
        type: types.SETARTICLE,
        data: {
            index: article.index,
            remote_id: article.remote_id,
            security: article.security,
            image_article_cover: article.image_article_cover,
            author: author,
            title: article.title,
            category: article.category,
            introduction: article.introduction,
            text: article.text,
            gallery: article.gallery,
            firstInCategory: article.firstInCategory,
            lastInCategory: article.lastInCategory,
            categoryIndex: article.categoryIndex,
        }
    }
}

export function setArticleFolder(article) {
    return {
        type: types.SETARTICLEFOLDER,
        data: {
            remote_id: article.remote_id,
            security: article.security,
            introduction: article.introduction,
            text: article.text,
            author: article.author[0],
            gallery: article.gallery,
        }
    }
}