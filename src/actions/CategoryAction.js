import * as types from './types/CategoryType';

export function setCategory(articles, index, title, articleIndex) {
    return {
        type: types.SETCATEGORY,
        data: {
            articles: articles,
            index: index,
            title: title,
            articleIndex: articleIndex
        }
    }
}
