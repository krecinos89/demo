import * as types from '../actions/types/AppType';

const initialState = {
    catalogue: null,
    catalogueName: null,
    categories: null,
    articles: null,
    products: null,
};

const AppReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SETCATALOGUE :
            return {
                ...state,
                catalogue: action.data.catalogue
            };
        case types.SETCATALOGUENAME :
            return {
                ...state,
                catalogueName: action.data.catalogueName
            };
        case types.SETCATEGORIES :
            return {
                ...state,
                categories: action.data.categories
            };
        case types.SETARTICLES :
            return {
                ...state,
                articles: action.data.articles
            };
        case types.SETPRODUCTS :
            return {
                ...state,
                products: action.data.products
            };
        default:
            return state
    }
};

export default AppReducer