import * as types from '../actions/types/CategoryType';

const initialState = {
    articles: null,
    index: null,
    title: null,
    articleIndex: null,
};

const CategoryReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SETCATEGORY :
            return {
                ...state,
                articles: action.data.articles,
                index: action.data.index,
                title: action.data.title,
                articleIndex: action.data.articleIndex,
            };
        default:
            return state
    }
};

export default CategoryReducer