import * as types from '../actions/types/ProductType';

const initialState = {
    product: null
};

const ProductReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SETPRODUCT :
            return {
                ...state,
            };
        default:
            return state
    }
};

export default ProductReducer