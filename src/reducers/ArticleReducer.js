import * as types from '../actions/types/ArticleType';

const initialState = {
    index: null,
    remote_id: null,
    security: null,
    title: null,
    category: null,
    introduction: null,
    text: null,
    image_article_cover: null,
    author: null,
    gallery: [],
    firstInCategory: null,
    lastInCategory: null,
    categoryIndex: null,
};

const ArticleReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SETARTICLE :
            return {
                ...state,
                index: action.data.index,
                remote_id: action.data.remote_id,
                security: action.data.security,
                image_article_cover: action.data.image_article_cover,
                title: action.data.title,
                author: action.data.author,
                category: action.data.category,
                introduction: action.data.introduction,
                text: action.data.text,
                gallery: action.data.gallery,
                firstInCategory: action.data.firstInCategory,
                lastInCategory: action.data.lastInCategory,
                categoryIndex: action.data.categoryIndex,
            };
        case types.SETARTICLEFOLDER :
            return {
                ...state,
                remote_id: action.data.remote_id,
                security: action.data.security,
                introduction: action.data.introduction,
                text: action.data.text,
                author: action.data.author,
                gallery: action.data.gallery,
            };
        default:
            return state
    }
};

export default ArticleReducer