import {combineReducers} from 'redux';

import AppReducer from './AppReducer';
import ArticleReducer from './ArticleReducer';
import ProductReducer from './ProductReducer';
import CategoryReducer from './CategoryReducer';

const allReducers = combineReducers({
    app: AppReducer,
    article: ArticleReducer,
    product: ProductReducer,
    category: CategoryReducer
});

export default allReducers