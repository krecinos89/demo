import {connect} from 'react-redux'

import * as CategoryAction from '../actions/CategoryAction';

import CategoryIndex from '../components/CategoryIndex'

const mapStateToProps = (state) => {
    return {
        state: {
            app: state.app,
            article: state.article,
            category: state.category
        }
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setCategory: (category) => {
            let articlesCategory = Object.keys(category.articles).map((key) => category.articles[key]);
            let articleTemp = [];
            let articleIndex = category.articleIndex;

            articlesCategory.map((article, key) => {
                articleTemp.push({
                    ...article,
                    category: category.name,
                    index: articleIndex + key
                });

                return null;
            });
            dispatch(CategoryAction.setCategory(articleTemp, category.index, category.name, category.articleIndex));
        },
    }
};

const CategoryContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CategoryIndex);

export default CategoryContainer
