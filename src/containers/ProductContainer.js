import {connect} from 'react-redux'

import * as ProductActions from '../actions/ProductAction';

import ProductContent from '../components/ProductContent'

const mapStateToProps = (state) => {
    return {
        state: {
            app: state.app,
            product: state.product,
        }
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setProduct: (product) => {
            dispatch(ProductActions.setProduct(product));
        },
    }
};

const productContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductContent);

export default productContainer
