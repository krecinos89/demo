import {connect} from 'react-redux'

import * as AppActions from '../actions/AppActions';
import * as ApiConfig from '../config/apiConfig';

import App from '../components/App'

import {getUserSecurity} from "../helpers/SecurityHelper"

const base64 = require('base-64');

const mapStateToProps = (state) => {
    return {
        state: {
            app: state.app,
        }
    }
};

const mapDispatchToProps = (dispatch) => {

    let headers = new Headers();
    headers.append("Authorization", "Basic " + base64.encode(ApiConfig.API_USER + ":" + ApiConfig.API_PWD));

    return {
        getCatalogue: (idCatalogue) => {

            fetch(ApiConfig.API_ROOT + '/webservices/jsonCatalogue/' + idCatalogue, {headers: headers})
                .then((response) => response.json())
                .then((responseJson) => {
                    // set tout le catalogue
                    let catalogue = Object.keys(responseJson).map((key) => responseJson[key]);
                    dispatch(AppActions.setCatalogue(catalogue));
                    dispatch(AppActions.setCatalogueName(catalogue[0]));

                    //set User Security for this catalogue
                    getUserSecurity(new Date(catalogue[2]));

                    // set un tableau de categorie
                    let categories = Object.keys(catalogue[1]).map((key) => catalogue[1][key]);
                    let categoriesTemp = categories;

                    // set un tableau avec tous les articles
                    let articles = [];
                    let products = [];
                    let articlesCategory = null;
                    let productsCategory = null;
                    let articleIndex = 0;
                    let productIndex = 0;
                    let firstArticleInCategoryIndex = 0;
                    let firstProductInCategoryIndex = 0;
                    categories.map((category, key) => {
                        articlesCategory = Object.keys(category.articles).map((key) => category.articles[key]);

                        if (articlesCategory.length === 0) {
                            categoriesTemp[key] = {
                                ...categoriesTemp[key],
                                articleIndex: null,
                                lastArticleIndex: null,
                                index: key,
                            };
                        } else {
                            categoriesTemp[key] = {
                                ...categoriesTemp[key],
                                articleIndex: firstArticleInCategoryIndex,
                                lastArticleIndex: firstArticleInCategoryIndex + articlesCategory.length - 1,
                                index: key,
                            };
                            firstArticleInCategoryIndex += articlesCategory.length;
                        }

                        articlesCategory.map((article) => {
                            article = {
                                ...article,
                                category: category.name,
                                categoryIndex: key,
                                index: articleIndex,
                                firstInCategory: (articleIndex === firstArticleInCategoryIndex - articlesCategory.length),
                                lastInCategory: (articleIndex === firstArticleInCategoryIndex - 1),
                            };
                            articleIndex++;
                            articles.push(article);
                            return null;
                        });

                        productsCategory = Object.keys(category.products).map((key) => category.products[key]);

                        if (productsCategory.length === 0) {
                            categoriesTemp[key] = {
                                ...categoriesTemp[key],
                                productIndex: null,
                                lastProductIndex: null,
                            };
                        } else {
                            categoriesTemp[key] = {
                                ...categoriesTemp[key],
                                productIndex: firstProductInCategoryIndex,
                                lastProductIndex: firstProductInCategoryIndex = productsCategory.length - 1,
                            };
                            firstProductInCategoryIndex += productsCategory.length;
                        }

                        productsCategory.map((product, keyProduct) => {
                            product = {
                                ...product,
                                category: category.name,
                                categoryIndex: key,
                                index: productIndex,
                                firstInCategory: (keyProduct === 0),
                                lastInCategory: (keyProduct === productsCategory.length - 1),
                            };
                            productIndex++;
                            products.push(product);
                            return null;
                        });

                        return null;
                    });
                    dispatch(AppActions.setCategories(categoriesTemp));
                    dispatch(AppActions.setArticles(articles));
                    dispatch(AppActions.setProducts(products));
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        connectByCookie: (cookie) => {
            localStorage.setItem("UserCookie", cookie);
            fetch(ApiConfig.API_ROOT + '/webservices/user/webapp/user/' + cookie, {headers: headers})
                .then((response) => response.json())
                .then((responseJson) => {
                    localStorage.setItem("UserEmail", responseJson.email);
                    localStorage.setItem("UserFirstName", responseJson.firstName);
                    localStorage.setItem("UserLastName", responseJson.lastName);
                    localStorage.setItem("UserFullName", responseJson.fullName);
                    localStorage.setItem("UserPurchases", JSON.stringify(responseJson.purchases));
                    localStorage.setItem("UserImageUrl", responseJson.image_url);

                    let bookmarks = Object.keys(responseJson.preferences.bookmarks).map((key) => responseJson.preferences.bookmarks[key]);
                    let remoteIds = bookmarks.map((bookmark) => {
                        return bookmark.remote_id
                    });
                    localStorage.setItem("UserBookmark", JSON.stringify(remoteIds));
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        disconnect: () => {
            localStorage.removeItem("UserCookie");
            localStorage.removeItem("UserEmail");
            localStorage.removeItem("UserFirstName");
            localStorage.removeItem("UserLastName");
            localStorage.removeItem("UserFullName");
            localStorage.removeItem("UserPurchases");
            localStorage.removeItem("UserImageUrl");
            localStorage.removeItem("UserBookmark");
        },
    }
};

const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(App);

export default AppContainer
