import {connect} from 'react-redux'

import * as ArticleActions from '../actions/ArticleActions';
import * as ApiConfig from '../config/apiConfig';

import ArticleContent from '../components/ArticleContent'

const base64 = require('base-64');

const mapStateToProps = (state) => {
    return {
        state: {
            app: state.app,
            article: state.article,
        }
    }
};

const mapDispatchToProps = (dispatch) => {

    let headers = new Headers();
    headers.append("Authorization", "Basic " + base64.encode(ApiConfig.API_USER + ":" + ApiConfig.API_PWD));

    return {
        setArticle: (article) => {
            dispatch(ArticleActions.setArticle(article));
        },
        setArticleFolder: (article) => {
            dispatch(ArticleActions.setArticleFolder(article));
        },
        addBookmark: (email, remoteId) => {
            fetch(ApiConfig.API_ROOT + '/webservices/user/bookmarks/add/' + email + '/' + remoteId, {headers: headers})
                .then(() => {
                    let bookmarks = JSON.parse(localStorage.getItem("UserBookmark"));
                    bookmarks.push(remoteId);
                    localStorage.setItem("UserBookmark", JSON.stringify(bookmarks));
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        deleteBookmark: (email, remoteId) => {
            fetch(ApiConfig.API_ROOT + '/webservices/user/bookmarks/remove/' + email + '/' + remoteId, {headers: headers})
                .then(() => {
                    let bookmarks = JSON.parse(localStorage.getItem("UserBookmark"));
                    if (bookmarks !== null && bookmarks.indexOf(remoteId) >= 0) {
                        bookmarks.splice(bookmarks.indexOf(remoteId), 1);
                    }
                    localStorage.setItem("UserBookmark", JSON.stringify(bookmarks));
                })
                .catch((error) => {
                    console.error(error);
                });
        }
    }
};

const ArticleContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ArticleContent);

export default ArticleContainer
