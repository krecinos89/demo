import React from 'react';
import ReactDOM from 'react-dom';

import {createStore} from 'redux';
import {Provider} from 'react-redux';
import {CookiesProvider} from 'react-cookie';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

import AppContainer from './containers/AppContainer';
import ArticleContainer from "./containers/ArticleContainer";
import ProductContainer from "./containers/ProductContainer";
import CategoryContainer from "./containers/CategoryContainer";

import Default from "./components/Default";

import allReducers from './reducers';
import './css/index.css';

const store = createStore(
    allReducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
    <Provider store={store}>
        <CookiesProvider>
            <Router>
                <Switch>
                    <Route exact path='/' component={Default}/>
                    <Route exact path='/edition/:numEdition' component={AppContainer}/>
                    <Route exact path='/edition/:numEdition/article/:numArticle' component={ArticleContainer}/>
                    <Route exact path='/edition/:numEdition/product/:numProduct' component={ProductContainer}/>
                    <Route exact path='/edition/:numEdition/category/:numCategory' component={CategoryContainer}/>
                </Switch>
            </Router>
        </CookiesProvider>
    </Provider>,
    document.getElementById('root')
);
